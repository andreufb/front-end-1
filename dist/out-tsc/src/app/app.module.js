import { __decorate } from "tslib";
import { QuestionService } from './general_components/dynamic-form/question.service';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
// Material
import { MatSliderModule } from '@angular/material/slider';
import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatTabsModule } from '@angular/material/tabs';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatRadioModule } from '@angular/material/radio';
import { MatDialogModule } from '@angular/material/dialog';
import 'hammerjs';
// Components
import { ToolBarComponent } from './general_components/tool-bar/tool-bar.component';
import { FooterComponent } from './general_components/footer/footer.component';
import { HomeComponent } from './general_components/components/home/home.component';
import { GetDataComponent } from './general_components/components/get-data/get-data.component';
import { EndpointDetailsComponent } from './general_components/components/endpoints_details/endpoint-details.component';
import { SelectedEndpointsComponent } from './general_components/components/selected-endpoints/selected-endpoints.component';
import { SendToApiComponent } from './general_components/components/send-to-api/send-to-api.component';
import { OptionsComponent } from './general_components/components/options/options.component';
import { LoginComponent } from './general_components/components/login/login.component';
// Services
import { SelectedEndpointsService } from './general_components/components/selected-endpoints/selected-endpoints.service';
import { LoginService } from './general_components/components/login/login.service';
import { SliderComponent } from './general_components/slider/slider.component';
import { LoginGuard } from './general_components/components/login/login.guard';
import { GetDataService } from './general_components/components/get-data/get-data.service';
import { DynamicFormQuestionComponent } from './general_components/dynamic-form/dynamic-form-question.component';
import { DynamicFormDialogComponent } from './general_components/dynamic-form/dynamic-form-dialog.component';
import { DynamicFormComponent } from './general_components/dynamic-form/dynamic-form.component';
let AppModule = class AppModule {
};
AppModule = __decorate([
    NgModule({
        declarations: [
            AppComponent,
            ToolBarComponent,
            FooterComponent,
            HomeComponent,
            GetDataComponent,
            EndpointDetailsComponent,
            SelectedEndpointsComponent,
            SendToApiComponent,
            OptionsComponent,
            LoginComponent,
            SliderComponent,
            DynamicFormQuestionComponent,
            DynamicFormDialogComponent,
            DynamicFormComponent
        ],
        imports: [
            BrowserModule,
            AppRoutingModule,
            HttpClientModule,
            FormsModule,
            NgbModule,
            ReactiveFormsModule,
            FlexLayoutModule,
            MatSliderModule,
            MatButtonModule,
            MatToolbarModule,
            MatCardModule,
            MatIconModule,
            MatFormFieldModule,
            MatInputModule,
            MatSelectModule,
            MatTabsModule,
            MatSnackBarModule,
            MatRadioModule,
            MatDialogModule,
            BrowserAnimationsModule
        ],
        providers: [
            SelectedEndpointsService,
            LoginService,
            LoginGuard,
            GetDataService,
            QuestionService
        ],
        bootstrap: [AppComponent],
        entryComponents: [DynamicFormDialogComponent]
    })
], AppModule);
export { AppModule };
//# sourceMappingURL=app.module.js.map