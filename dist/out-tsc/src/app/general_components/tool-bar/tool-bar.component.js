import { __decorate } from "tslib";
import { Component } from '@angular/core';
let ToolBarComponent = class ToolBarComponent {
    constructor() { }
    ngOnInit() {
    }
    logout() {
        localStorage.clear();
    }
};
ToolBarComponent = __decorate([
    Component({
        selector: 'app-tool-bar',
        templateUrl: './tool-bar.component.html',
        styleUrls: ['./tool-bar.component.css']
    })
], ToolBarComponent);
export { ToolBarComponent };
//# sourceMappingURL=tool-bar.component.js.map