import { __decorate } from "tslib";
import { Component } from '@angular/core';
import { endpoints } from '../templates';
let EndpointDetailsComponent = class EndpointDetailsComponent {
    constructor(route, selectedEndpointsService) {
        this.route = route;
        this.selectedEndpointsService = selectedEndpointsService;
        this.url = "http://147.83.159.160:5000/";
    }
    ngOnInit() {
        this.route.paramMap.subscribe(params => {
            this.endpoint = endpoints[+params.get('endpointName')];
        });
    }
    ;
    addToSelected(endpoint) {
        this.selectedEndpointsService.addToSelected(endpoint);
        window.alert('Endpoint added to selected');
    }
};
EndpointDetailsComponent = __decorate([
    Component({
        selector: 'app-endpoint-details',
        templateUrl: './endpoint-details.component.html',
        styleUrls: ['./endpoint-details.component.css']
    })
], EndpointDetailsComponent);
export { EndpointDetailsComponent };
//# sourceMappingURL=endpoint-details.component.js.map