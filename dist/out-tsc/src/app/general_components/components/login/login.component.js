import { __decorate } from "tslib";
import { Component } from '@angular/core';
let LoginComponent = class LoginComponent {
    constructor(loginService, formBuilder) {
        this.loginService = loginService;
        this.formBuilder = formBuilder;
        this.token = "";
        this.has_token = false;
        this.loginForm = this.formBuilder.group({
            email: '',
            password: ''
        });
    }
    ;
    ngOnInit() {
        this.has_token = this.loginService.isLoggedIn;
    }
    ;
    onSubmit(customerData) {
        this.loginService.getToken(customerData);
        this.loginForm.reset();
    }
    ;
};
LoginComponent = __decorate([
    Component({
        selector: 'app-login',
        templateUrl: './login.component.html',
        styleUrls: ['./login.component.css']
    })
], LoginComponent);
export { LoginComponent };
//# sourceMappingURL=login.component.js.map