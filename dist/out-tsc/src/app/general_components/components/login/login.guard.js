import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
let LoginGuard = class LoginGuard {
    constructor(loginService, snackBar, router) {
        this.loginService = loginService;
        this.snackBar = snackBar;
        this.router = router;
    }
    canActivate(next, state) {
        if (!this.loginService.isLoggedIn) {
            this.snackBar.open(`You are not logged`, 'Logg in for access', {
                duration: 6000,
                panelClass: 'snackbar-success',
                verticalPosition: 'top',
                horizontalPosition: 'center'
            });
            this.router.navigate(['/login']);
        }
        return this.loginService.isLoggedIn;
    }
};
LoginGuard = __decorate([
    Injectable({
        providedIn: 'root'
    })
], LoginGuard);
export { LoginGuard };
//# sourceMappingURL=login.guard.js.map