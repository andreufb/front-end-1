import { __decorate } from "tslib";
import { Component } from '@angular/core';
let SelectedEndpointsComponent = class SelectedEndpointsComponent {
    constructor(selectedEndpointsService, formBuilder) {
        this.selectedEndpointsService = selectedEndpointsService;
        this.formBuilder = formBuilder;
        this.checkoutForm = this.formBuilder.group({
            email: '',
            password: ''
        });
    }
    ;
    ngOnInit() {
        this.endpoints = this.selectedEndpointsService.getEndpoints();
    }
    ;
    onSubmit(customerData) {
        this.endpoints = this.selectedEndpointsService.clearSelected();
        this.checkoutForm.reset();
        console.warn('Getting the data...', customerData);
    }
    ;
};
SelectedEndpointsComponent = __decorate([
    Component({
        selector: 'app-selected-endpoints',
        templateUrl: './selected-endpoints.component.html',
        styleUrls: ['./selected-endpoints.component.css']
    })
], SelectedEndpointsComponent);
export { SelectedEndpointsComponent };
//# sourceMappingURL=selected-endpoints.component.js.map