import { __awaiter, __decorate } from "tslib";
import { QuestionControlService } from './../../dynamic-form/question-control.service';
import { Component } from '@angular/core';
import { Input } from '@angular/core';
import { Output, EventEmitter } from '@angular/core';
let GetDataComponent = class GetDataComponent {
    constructor(qcs, _snackBar, questionService, getDataService) {
        this.qcs = qcs;
        this._snackBar = _snackBar;
        this.questionService = questionService;
        this.getDataService = getDataService;
        this.questions = this.questionService.getData();
        this.close = new EventEmitter();
        this.number_consulta = -1;
    }
    ngOnInit() {
        this.form = this.qcs.toFormGroup(this.questions);
    }
    disponibleID(event) {
        this.disponibleIDs = event.target.value;
    }
    getData() {
        return new Promise((resolve) => {
            this.payLoad = JSON.parse(JSON.stringify(this.form.value));
            this.response = this.getDataService.getData(this.payLoad);
            this.number_consulta += 1;
            resolve(this.response);
        });
    }
    ;
    displayText() {
        this._snackBar.open(`Submited Successfully`, '', {
            duration: 4000,
            panelClass: 'snackbar-success',
            verticalPosition: 'top',
            horizontalPosition: 'center'
        }).afterOpened().subscribe(() => {
            this.message = this.response;
            console.log(this.response);
        });
    }
    onSubmit() {
        return __awaiter(this, void 0, void 0, function* () {
            yield this.getData().then((res) => {
                this.message = res;
                // this.displayText()
            });
        });
    }
    // onSubmit() {
    //   this._snackBar.open(`Submited Successfully`, '', {
    //     duration: 4000,
    //     panelClass: 'snackbar-success',
    //     verticalPosition: 'top',
    //     horizontalPosition: 'center'
    //   }).afterOpened().subscribe(() => {
    //     this.payLoad = JSON.parse(JSON.stringify(this.form.value));
    //     this.response = this.getDataService.getData(this.payLoad);
    //     this.number_consulta += 1;
    //   });
    // }
    // displayText() {
    //   if (this.response != []) {
    //     this.textarea = document.getElementById("display");
    //     this.textarea.value = JSON.stringify(this.response[this.number_consulta], null, 2);
    //   }
    // }
    displayAllText() {
        if (this.response != []) {
            this.textarea = document.getElementById("display");
            this.textarea.value = JSON.stringify(this.response, null, 2);
        }
    }
    refresh() {
        this.response = [];
        window.location.assign('http://147.83.159.160:4200/home/getData');
    }
};
__decorate([
    Input()
], GetDataComponent.prototype, "isDialog", void 0);
__decorate([
    Output()
], GetDataComponent.prototype, "close", void 0);
GetDataComponent = __decorate([
    Component({
        selector: 'app-get-data',
        templateUrl: './get-data.component.html',
        styleUrls: ['./get-data.component.css'],
        providers: [QuestionControlService]
    })
], GetDataComponent);
export { GetDataComponent };
//# sourceMappingURL=get-data.component.js.map