import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { throwError } from 'rxjs';
import { catchError, retry } from 'rxjs/operators';
const baseURL = 'http://147.83.159.160:5000/api/';
let GetDataService = class GetDataService {
    constructor(http, snackBar, router) {
        this.http = http;
        this.snackBar = snackBar;
        this.router = router;
        this.response = [];
        this.httpOptions = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
            })
        };
    }
    getData(data) {
        this.endpoint = baseURL + data.endpoint;
        this.id = data.id;
        this.url = this.endpoint + '/' + this.id;
        if (this.id == null) {
            this.url = this.endpoint + '/';
        }
        this.http.get(this.url, this.httpOptions).pipe(retry(1), catchError(this.errorHandl)).subscribe(data => {
            this.response.push(JSON.parse(JSON.stringify(data)));
        });
        return this.response[this.response.length - 1];
    }
    ;
    errorHandl(error) {
        let errorMessage = 'Email or password incorrect';
        localStorage.setItem('loggedIn', 'false');
        localStorage.setItem('token', 'null');
        // if (error.error instanceof ErrorEvent) {
        //   // Get client-side error
        //   errorMessage = error.error.message;
        // } else {
        //   // Get server-side error
        //   errorMessage = `Error Code: ${error.status}\nMessage: ${error.message}`;
        // }
        console.log(errorMessage);
        this.snackBar.open(`Acces not granted`, 'User or passwrod incorrect', {
            duration: 6000,
            panelClass: 'snackbar-success',
            verticalPosition: 'top',
            horizontalPosition: 'center'
        });
        return throwError(errorMessage);
    }
    ;
};
GetDataService = __decorate([
    Injectable({
        providedIn: 'root'
    })
], GetDataService);
export { GetDataService };
//# sourceMappingURL=get-data.service.js.map