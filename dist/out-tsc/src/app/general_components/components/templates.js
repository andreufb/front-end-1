export const endpoints = [
    {
        name: "Person",
        endpoint: "person",
        information: ["idperson", "name", "mail", "role"],
        about: "SARTI's personal name, mail and role"
    },
    {
        name: "Mission",
        endpoint: "mission",
        information: ["idmission", "label", "description"],
        about: "SARTI's missions"
    },
    {
        name: "Action",
        endpoint: "action",
        information: ["idaction", "label", "description", "uuid", "time", "actionTime", "idphysicalSystem", "idproject"],
        about: "SARTI's research group actions"
    }
];
export const options = [
    {
        url: "login",
        name: "Login",
        hint: "Required to use the web",
        disabled: false,
    },
    {
        url: "getData",
        name: "Get Data",
        disabled: true,
    },
    {
        url: "putData",
        name: "Put Data",
        disabled: true,
    },
    {
        url: "updateData",
        name: "Update Data",
        disabled: true,
    }
];
//# sourceMappingURL=templates.js.map