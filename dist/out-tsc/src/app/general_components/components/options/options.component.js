import { __decorate } from "tslib";
import { Component } from '@angular/core';
import { options } from './../templates';
let OptionsComponent = class OptionsComponent {
    constructor(loginService) {
        this.loginService = loginService;
        this.options = options;
        this.base_url = "http://147.83.159.160:4200/home/";
        this.has_token = false;
    }
    ngOnInit() {
        this.has_token = this.loginService.isLoggedIn;
    }
    getURL(option) {
        var url;
        url = JSON.parse(JSON.stringify(option));
        window.location.assign(this.base_url + url.url);
    }
};
OptionsComponent = __decorate([
    Component({
        selector: 'app-options',
        templateUrl: './options.component.html',
        styleUrls: ['./options.component.css']
    })
], OptionsComponent);
export { OptionsComponent };
//# sourceMappingURL=options.component.js.map