import { __decorate } from "tslib";
import { Component, Input } from '@angular/core';
let DynamicFormQuestionComponent = class DynamicFormQuestionComponent {
    get isValid() { return this.form.controls[this.question.key].valid; }
    get endpoint() { return this.form.controls[this.question.endpoint]; }
};
__decorate([
    Input()
], DynamicFormQuestionComponent.prototype, "question", void 0);
__decorate([
    Input()
], DynamicFormQuestionComponent.prototype, "form", void 0);
DynamicFormQuestionComponent = __decorate([
    Component({
        selector: 'app-question',
        templateUrl: './dynamic-form-question.component.html',
        styleUrls: ['./dynamic-form-question.component.css'],
    })
], DynamicFormQuestionComponent);
export { DynamicFormQuestionComponent };
//# sourceMappingURL=dynamic-form-question.component.js.map