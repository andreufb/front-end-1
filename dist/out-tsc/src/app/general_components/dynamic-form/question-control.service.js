import { __decorate } from "tslib";
import { Injectable } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
let QuestionControlService = class QuestionControlService {
    constructor() { }
    toFormGroup(questions) {
        let group = {};
        //questions.forEach(question => {
        //  group[question.key] = question.required ? new FormControl(question.value, Validators.required)
        //                                          : new FormControl(question.value || '');
        //});
        questions.forEach(question => {
            group[question.key] = question.required ? new FormControl()
                : new FormControl();
        });
        return new FormGroup(group);
    }
};
QuestionControlService = __decorate([
    Injectable()
], QuestionControlService);
export { QuestionControlService };
//# sourceMappingURL=question-control.service.js.map