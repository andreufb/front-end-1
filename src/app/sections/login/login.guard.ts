import { LoginService } from './login.service';
import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class LoginGuard implements CanActivate {

  constructor(
    private loginService: LoginService,
    private snackBar: MatSnackBar,
    private router: Router
    ) { }

  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      if (!this.loginService.isLoggedIn) {
        this.snackBar.open(`You are not logged`, 'Logg in for access', {
          duration: 6000,
          panelClass: 'snackbar-success',
          verticalPosition: 'top',
          horizontalPosition: 'center'
        });
        this.router.navigate(['login'])
      }

    return this.loginService.isLoggedIn;
  }
}