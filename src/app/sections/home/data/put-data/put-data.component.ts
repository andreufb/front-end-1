import { PutDataService } from './put-data.service';
import { Component, OnInit, ViewEncapsulation } from '@angular/core';

@Component({
  selector: 'app-put-data',
  templateUrl: './put-data.component.html',
  styleUrls: ['./put-data.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class PutDataComponent implements OnInit {

  constructor(private putDataService: PutDataService) { }

  ngOnInit(): void {
  }

}
