import { Router } from '@angular/router';
import { ButtonSettings } from '../../../tools/button/button-settings';
import { Component, OnInit } from '@angular/core';
import { LoginService } from '../../login/login.service';

@Component({
  selector: 'app-data',
  templateUrl: './data.component.html',
  styleUrls: ['./data.component.css']
})
export class DataComponent implements OnInit {


  parent= "/home/data"
  has_token = false;

  back_button: ButtonSettings = {
    'disabled': false,
    'label': "Go back",
    'type': "sidenav"
  }

  sidenavButtonSettings: Array<ButtonSettings> = [{
    'disabled': false,
    'label': "Get data",
    'type': "sidenav",
    'route': 'getData'
  },
  {
    'disabled': false,
    'label': "Put Data",
    'type': "sidenav",
    'route': 'putData'
  },
  {
    'disabled': false,
    'label': "Update data",
    'type': "sidenav",
    'route': 'updateData'
  },
  {
    'disabled': false,
    'label': "Configure",
    'type': "sidenav",
    'route': "configure"
  }
  ];

  constructor(private loginService: LoginService, public router: Router) {

  }

  ngOnInit(): void {
    this.has_token = this.loginService.isLoggedIn
  }


}
