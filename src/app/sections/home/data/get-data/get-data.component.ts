import { Router } from '@angular/router';
import { GetDataService } from './get-data.service';
import { QuestionService } from '../../../../tools/dynamic-form/question.service';
import { QuestionControlService } from '../../../../tools/dynamic-form/question-control.service';
import { QuestionBase } from '../../../../tools/dynamic-form/question-base';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Component, OnInit } from '@angular/core';
import { Input } from '@angular/core';
import { Output, EventEmitter } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { windowWhen } from 'rxjs/operators';


@Component({
  selector: 'app-get-data',
  templateUrl: './get-data.component.html',
  styleUrls: ['./get-data.component.css'],
  providers: [QuestionControlService]
})



export class GetDataComponent implements OnInit {

  questions: QuestionBase<any>[] = this.questionService.getData();
  @Input() isDialog: boolean;

  @Output() close = new EventEmitter<any>();

  form: FormGroup;
  payLoad;
  response;
  number_consulta = -1;
  disponibleIDs;
  textarea;
  message;
  download_name = 'SARTI_data.json';


  constructor(private qcs: QuestionControlService, private _snackBar: MatSnackBar, private questionService: QuestionService, private getDataService: GetDataService, public route: Router) { }

  ngOnInit() {
    this.form = this.qcs.toFormGroup(this.questions);
  }

  disponibleID(event: any) {
    this.disponibleIDs = event.target.value;
  }


  getData() {
    return new Promise((resolve) => {
      this.payLoad = JSON.parse(JSON.stringify(this.form.value));
      this.download_name = 'SARTI_' + this.payLoad.endpoint + '.json'
      resolve(this.getDataService.getData(this.payLoad))
    })
  };

  onSubmit() {
    this._snackBar.open(`Submited Successfully`, '', {
      duration: 4000,
      panelClass: 'snackbar-success',
      verticalPosition: 'top',
      horizontalPosition: 'center'
    }).afterOpened().subscribe(() => {
      this.getData().then((res) => {
        this.textarea = document.getElementById("display");
        this.textarea.value = JSON.stringify(res, null, 2);
      }
      );
    });
  }

  displayAllText() {
    this.download_name = 'SARTI_all_data.json'
    this.textarea.value = JSON.stringify(this.getDataService.getAllData(), null, 2);
  }

  download() {
    var json = this.textarea.value
    json = [json]
    var blob = new Blob(json, { type: "text/plain;charset=urf-8" });
    var isIE = false
    if (isIE) {
      window.navigator.msSaveBlob(blob, this.download_name);
    } else {
      var url = window.URL || window.webkitURL;
      var link = url.createObjectURL(blob);
      var a = document.createElement("a");
      a.download = this.download_name;
      a.href = link;
      document.body.appendChild(a);
      a.click();
      document.body.removeChild(a);
    }


  }


  refresh() {
    this.response = [];
    window.location.assign(this.route.url)
  }


}

