import { FunctionCall } from '@angular/compiler';
import { ButtonSettings } from '../../../../../tools/button/button-settings';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';
import { windowWhen } from 'rxjs/operators';


@Component({
  selector: 'app-sensor',
  templateUrl: './sensor.component.html',
  styleUrls: ['./sensor.component.css'],
  providers: [{
    provide: STEPPER_GLOBAL_OPTIONS, useValue: { showError: true }
  }]
})
export class SensorComponent implements OnInit {
  sensorMetadata_FormGroup: FormGroup;
  observableProperties_FormGroup: FormGroup;
  units_FormGroup: FormGroup;
  contacts_FormGroup: FormGroup;
  principalInvestigator_FormGroup: FormGroup;
  operator_FormGroup: FormGroup;
  owner_FormGroup: FormGroup;
  station_FormGroup: FormGroup;


  payload = {
    "sensorMetadata": {},
    "observableProperties": [],
    "contacts": {},
    "deployment": {
      "station": {}
    }
  };

  settings: ButtonSettings[] = [{
    'disabled': false,
    'label': "Reset",
    'type': "delete"
  },
  {
    'disabled': false,
    'label': "Delete last",
    'type': "delete"
  },
  {
    'disabled': false,
    'label': "Add",
    'type': "add"
  },
  {
    'disabled': false,
    'label': "Watch configuration",
    'type': "next"
  },
  {
    'disabled': false,
    'label': "Download",
    'type': "add"
  },
  {
    'disabled': false,
    'label': "Send",
    'type': "next"
  }
  ];

  


  show_payload = false;

  constructor(private _formBuilder: FormBuilder) { }

  ngOnInit() {
    this.sensorMetadata_FormGroup = this._formBuilder.group({
      shortName: '',
      longName: '',
      model: '',
      serialNumber: '',
      manufacturer: '',
      instrumentType: ''
    });

    this.observableProperties_FormGroup = this._formBuilder.group({
      name: ['', Validators.required],
      label: ['', Validators.required],
      definition: ['', Validators.required],
      units: {},
    });

    this.units_FormGroup = this._formBuilder.group({
      code: ['', Validators.required],
      href: ['', Validators.required]
    });

    this.contacts_FormGroup = this._formBuilder.group({
      principalInvestigator: {},
      operator: {},
      owner: {},
    });

    this.principalInvestigator_FormGroup = this._formBuilder.group({
      name: ['', Validators.required],
      mail: ['', Validators.required]
    });

    this.operator_FormGroup = this._formBuilder.group({
      name: ['', Validators.required],
      mail: ['', Validators.required]
    });

    this.owner_FormGroup = this._formBuilder.group({
      name: ["SARTI Research Group, Universitat Politècnica de Catalunya", Validators.required],
      EDMO_Code: [2150, Validators.required]
    });

    this.station_FormGroup = this._formBuilder.group({
      name: ['', Validators.required],
      id: ['', Validators.required],
      latitude: ['', Validators.required],
      longitude: ['', Validators.required],
      depth: ['', Validators.required],
      deploymentDate: ['', Validators.required]
    });

  };

  reset() {
    this.payload = {
      "sensorMetadata": {},
      "observableProperties": [],
      "contacts": {},
      "deployment": {
        "station": {}
      }
    };
  }

  show() {
    this.show_payload = true;
  };

  not_show() {
    this.show_payload = false;
  };

  send() {
    console.log(this.payload)
  };

  add_sensorMetadata() {
    this.payload.sensorMetadata = this.sensorMetadata_FormGroup.value;
  };

  add_observableProperty() {
    this.observableProperties_FormGroup.value.units = this.units_FormGroup.value;
    this.payload.observableProperties.push(this.observableProperties_FormGroup.value);
  };

  add_contacts() {
    this.contacts_FormGroup.value.principalInvestigator = this.principalInvestigator_FormGroup.value;
    this.contacts_FormGroup.value.operator = this.operator_FormGroup.value;
    this.contacts_FormGroup.value.owner = this.owner_FormGroup.value;
    this.payload.contacts = this.contacts_FormGroup.value;
  };

  add_deployment() {
    this.payload.deployment.station = this.station_FormGroup.value;
  };

  delete_observableProperty() {
    this.payload.observableProperties.pop();
  }

  download() {
    // @ts-ignore Ignoro la proxima linea de codi perque marca error al no trobar shortName (es defineix a l'stepper)
    var download_name = 'config_' + this.payload.sensorMetadata.shortName + '.json';
    var json = [JSON.stringify(this.payload)];
    var blob = new Blob(json, { type: "application/json;charset=urf-8" });
    var isIE = false
    if (isIE) {
      window.navigator.msSaveBlob(blob, download_name);
    } else {
      var url = window.URL || window.webkitURL;
      var link = url.createObjectURL(blob);
      var a = document.createElement("a");
      a.download = download_name;
      a.href = link;
      document.body.appendChild(a);
      a.click();
      document.body.removeChild(a);
    }


  }


}
